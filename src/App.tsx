import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootswatch/dist/yeti/bootstrap.min.css';
import TaskForm from './components/TaskForm';
import TaskList from './components/TaskList';
import { TasksProvider } from './context/TasksContext';

interface Props {
  title: string;
}

function App({ title }: Props) {
  console.log('Render App')
  return (
    <TasksProvider>
      <div className="App">

        <nav className="navbar navbar-dark bg-primary">
          <div className="container-fluid">
            <a className="navbar-brand" href="/">
              <img src={logo} alt="" width={40} className="d-inline-block align-text-top" />
                TODO LIST REACT AND TS
            </a>
          </div>
        </nav>
        <div className="container mt-5">
          <div className="row">
            <div className="col-lg-3">
              <TaskForm />
            </div>
            <div className="col-lg-9 mt-3 mt-xs-3 mt-sm-3 mt-lg-0">
              <TaskList />
            </div>
          </div>
        </div>
      </div>
    </TasksProvider>




  );
}

export default App;
