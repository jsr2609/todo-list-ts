
import React, { createContext, FC, ReactNode, useState } from 'react';
import { Task } from '../interfaces/Task';



const initialTasks: Task[] = [
  {
    id: 1,
    title: "Revisar TS",
    description: "Realizar el Hola Mundo",
    completed: false,
  },

  {
    id: 2,
    title: "Revisar React",
    description: "Realizar el Hola Mundo",
    completed: false,
  }
];

const defaultState = {
  tasks: initialTasks
}

export interface ITasksContexts {
  tasks: Task[];
  deleteTask?: (taskId: number) => void;
  addNewTask?: (task: Task) => void;
}
export const TasksContext = createContext<ITasksContexts>(defaultState)

interface Props {
  children: ReactNode
}

export const TasksProvider: FC<Props> = ({ children }) => {

  const [tasks, setTasks] = useState(initialTasks);
  const deleteTask = (taskId: number) => {
    setTasks(tasks.filter((task) => task.id !== taskId))
  }

  const addNewTask = (task: Task): void => {

    console.log(task);
    const date = new Date();
    const newTaskList = [{ ...task, id: date.getMilliseconds() }, ...tasks];
    setTasks(newTaskList);
  }

  return (
    <TasksContext.Provider value={{
      tasks,
      deleteTask,
      addNewTask
    }}>
      {children}
    </TasksContext.Provider>
  )
}