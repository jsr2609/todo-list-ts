import React,  {useContext, FormEvent, useState} from 'react'
import {TasksContext} from '../context/TasksContext'

const initialState = { title: "", description: ""}



const TaskForm = () => {
    console.log('Render TaskForm');

    const {addNewTask} = useContext(TasksContext);
    const [task, setTask] = useState(initialState);
    const handleChange = (e: FormEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        e.preventDefault();
        const element = e.currentTarget;
        
        const newTask = { ...task, [element.name]: element.value }
        setTask(newTask);
    }
    const handleSubmitForm = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        
        if(addNewTask) addNewTask(task);
        setTask(initialState);
        
    }
    return (
        <>
            <div>TaskForm</div>
            <div>
                <form onSubmit={handleSubmitForm} action="">

                    <div className="form-floating mb-3">
                        <input type="text" onChange={handleChange} className="form-control" id="title" name="title" placeholder="name@example.com" value={task.title} />
                        <label htmlFor="title">Title</label>
                    </div>

                    <div className="form-floating mb-3">
                        <textarea className="form-control" onChange={handleChange} placeholder="Leave a comment here" id="description" name="description" value={task.description}></textarea>
                        <label htmlFor="description">Description</label>
                    </div>

                    
                    <button className="btn btn-primary btn-block w-100">Enviar</button>
                </form>
            </div>

        </>

    )
}

export default TaskForm