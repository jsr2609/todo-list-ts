import React, { useContext } from 'react'
import { Task } from '../interfaces/Task'
import {TasksContext} from '../context/TasksContext'


interface Props {
    task: Task;
}

const TaskCard = ({task}: Props) => {
    console.log('Render TaskCard')
    const {deleteTask} = useContext(TasksContext);

    const handleDeleteTask = (taskId: number) => {
        if(deleteTask) deleteTask(taskId);
    }
    return (
        <div className="card m-1" style={{width: "18rem"}}>
                <div className="card-body"  >
                    <h5 className="card-title">{task.title}</h5>
                    <p className="card-text">{task.description}</p>
                    <button onClick={(e) => handleDeleteTask(task.id!)} className="btn btn-danger btn-block w-100">Eliminar</button>
                </div>
        </div>
    )
}

export default TaskCard