import React, {useContext} from 'react'
import TaskCard from './TaskCard'
import {TasksContext, ITasksContexts} from '../context/TasksContext'

const TaskList = () => {
    console.log('Render TaskList')
    const { tasks } = useContext<ITasksContexts>(TasksContext);
    return (
        <>
            <div>TaskList</div>
            <div className="d-flex flex-row flex-wrap justify-content-center">
                {
                    tasks.map(task => <TaskCard  key={task.id} task={task} />)
                }

            </div>
        </>

    )
}

export default TaskList


